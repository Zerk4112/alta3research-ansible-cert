# alta3research-ansible-cert

My submission for the alta3 certification exam

# Change IP on a VM in Proxmox using cloud-init
Basic Ansible Playbook to change the IP address of a VM in Proxmox using cloud-init.



## Requirements
- Server with Proxmox installed
- A VM with cloud-init and qemu-guest-tools installed.
- vars/pve_secrets.yml must be updated and encrypted.
- Ansible Collection: [community.general.proxmox_kvm ](https://docs.ansible.com/ansible/latest/collections/community/general/proxmox_kvm_module.html) must be installed.

## Usage

### Inventory Configuration

To try this playbook on your own, first you must define an inventory

~~~ ini
[proxmox]
<ip.address.of.your.proxmox.server>
~~~

### pve_secrets.yml Configuration

The pve_secrets.yml file should contain all of the information required to connect to your Proxmox server.

``` yml
pveuser: <api_username@pve>
pvepass: <api_password>
pvenode: <name_of_proxmox_node>
```

Be sure to encrypt this file!

``` sh
ansible-vault encrypt pve_secrets.yml
```

### Ansible Command

To execute this ansible playbook, a vault-id must be passed into the playbook to decrypt the pve_secrets.yml file used within.

``` sh
ansible-playbook alta3research-ansiblecert01.yml --vault-id @prompt
```

## Links & Credits
- Thanks Alta3 for the training opportunity!
- [community.general.proxmox_kvm Ansible Collection](https://docs.ansible.com/ansible/latest/collections/community/general/proxmox_kvm_module.html)
- StackOverflow
- [Proxmox Virtual Environment](https://www.proxmox.com/en/)



## Expected Output


``` bash
ansible-playbook alta3research-ansiblecert01.yml --vault-id @prompt
Vault password (default): 
Provide a vmid for the VM to be changed: 999
Provide a new IP address: 10.20.30.40
Provide a new gateway address: 10.20.30.1

PLAY [Change IP of Ubuntu VM on Proxmox with cloud-init] ************************************************************************************************************************************************************************************

TASK [Gathering Facts] **********************************************************************************************************************************************************************************************************************
ok: [proxmox_server]

TASK [Check VM state] ***********************************************************************************************************************************************************************************************************************
ok: [proxmox_server]

TASK [Display current_vm_info] **************************************************************************************************************************************************************************************************************
ok: [proxmox_server] => {
    "msg": {
        "changed": false,
        "failed": false,
        "msg": "VM applet with vmid = 999 is running",
        "status": "running",
        "vmid": 999
    }
}

TASK [Set proceed to yes] *******************************************************************************************************************************************************************************************************************
ok: [proxmox_server]

TASK [Change IP on existing VM on Proxmox without nameservers] ******************************************************************************************************************************************************************************
changed: [proxmox_server]

TASK [Display new_vm_info] ******************************************************************************************************************************************************************************************************************
ok: [proxmox_server] => {
    "msg": {
        "changed": true,
        "failed": false,
        "msg": "VM None with vmid 999 updated",
        "vmid": 999
    }
}

TASK [Change IP on existing VM on Proxmox with nameservers] *********************************************************************************************************************************************************************************
skipping: [proxmox_server]

TASK [Display new_vm_info] ******************************************************************************************************************************************************************************************************************
ok: [proxmox_server] => {
    "msg": {
        "changed": false,
        "skip_reason": "Conditional result was False",
        "skipped": true
    }
}

RUNNING HANDLER [Restart VM] ****************************************************************************************************************************************************************************************************************
changed: [proxmox_server]

PLAY RECAP **********************************************************************************************************************************************************************************************************************************
proxmox_server                       : ok=8    changed=2    unreachable=0    failed=0    skipped=1    rescued=0    ignored=0   

```
